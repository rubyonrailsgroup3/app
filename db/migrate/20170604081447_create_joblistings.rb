class CreateJoblistings < ActiveRecord::Migration[5.0]
	def change
		create_table :joblistings do |t|
			t.string :title, null: false
			t.text :description, null: false
			t.belongs_to :user, null:false, foreign_key: true
			
			t.timestamps null: false
		end
		
		add_index :joblistings, [:user_id, :title]
	end
end