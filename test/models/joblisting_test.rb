require 'test_helper'

class JoblistingTest < ActiveSupport::TestCase
  # set up model object to use
  let(:joblisting) { Joblisting.new() }

  test 'requires a user' do
    joblisting.errors[:user].must_include("can't be blank")
  end
  test 'requires a title' do
    joblisting.errors[:title].must_include("can't be blank")
  end
  test 'requires a description' do
    joblisting.errors[:description].must_include("can't be blank")
  end
  test 'requires the title to be unique for the same user' do
    existing_joblisting = create(:recipe)
    joblisting.name = existing_joblisting.name
    joblisting.user = existing_joblisting.user
    assert(joblisting.valid?)

    assert_not_nil(joblisting.errors[:title])
  end
  # test 'does not require the name to be unique for the different users'
end