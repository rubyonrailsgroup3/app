FROM ruby:2.3.0

RUN apt-get update -qq && apt-get install -y build-essential nodejs

ENV APP_HOME /jobapp
ENV PORT 4000
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

ADD Gemfile* $APP_HOME/	
RUN bundle install

ADD . $APP_HOME

RUN chmod 0777 -R $APP_HOME

RUN useradd -m myuser
USER myuser

#docker build . -t jobapp
#docker run -p 7000:4000 -e PORT=4000 jobapp
#docker-machine ip default
#http://ip:7000

#change this to be on development.
CMD kill -9 $(lsof -i tcp:5000 -t)
CMD rails db:migrate RAILS_ENV=development
CMD rails server --port $PORT --binding 0.0.0.0 -e development