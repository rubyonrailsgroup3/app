Rails.application.routes.draw do

  root 'frontpage#index'

  #resources :joblist
  get 'joblist' => 'joblist#index'
  post 'joblist' => 'joblist#create'
  put 'joblisting/:id' => 'joblist#update'

  resources :joblisting, only: %w(new edit show destroy), controller: "joblist"

  # clearance gem defines its own paths for user handling
end
