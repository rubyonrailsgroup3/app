class JoblistController < ApplicationController

  def index
    @jobs = Joblisting.all.order(:id)
  end

  def show
    @job = Joblisting.find(params[:id])
  end

  def new
    if !current_user.nil?
      @job = current_user.joblistings.build()
    else
      redirect_to joblist_path, notice: "Forbidden. Log in before attempting."
    end
  end

  def create
    if !current_user.nil?
      @job = current_user.joblistings.build(joblisting_params)

      if @job.save
        redirect_to joblist_path(@job), notice: 'Created job listing.'
      else
        @errors = @job.errors.full_messages
        render :new
      end
    else
      redirect_to joblist_path, notice: "Forbidden. Log in before attempting."
    end
  end

  def edit
    if !current_user.nil?
      @job = Joblisting.find(params[:id])

      if @job.user != current_user
        redirect_to joblist_path, notice: 'Forbidden.'
      end
    else
      redirect_to joblist_path, notice: 'Forbidden. Log in before attempting.'
    end
  end

  def update
    if !current_user.nil?
      @job = Joblisting.find(params[:id])

      if @job.user == current_user
        if @job.update_attributes(joblisting_params)
          redirect_to joblist_path(@job), notice: 'Updated job listing.'
        else
          @errors = @job.errors.full_messages
          render :edit
        end
      end
    else
      redirect_to joblist_path, notice: "Forbidden. Log in before attempting."
    end
  end

  def destroy
    if !current_user.nil?
      job = Joblisting.find(params[:id])

      if job.user == current_user
        job.destroy
        redirect_to joblist_path, notice: "Deleted job listing: #{job.title}"
      else
        redirect_to joblist_path, notice: "Forbidden."
      end
    else
      redirect_to joblist_path, notice: "Forbidden. Log in before attempting."
    end
  end

  private
  # validating a form, used for creating new job listings
  def joblisting_params
    params.require(:joblisting).permit(:title, :description)
  end
end
