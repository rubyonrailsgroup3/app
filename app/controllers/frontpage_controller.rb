class FrontpageController < ApplicationController
  def index
  	if !current_user.nil?
  		@jobs = current_user.joblistings.all.order(:id)
  	end
  end
end
