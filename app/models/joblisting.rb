class Joblisting < ApplicationRecord
  belongs_to :user
  validates_presence_of  :title, uniqueness: true
  validates_presence_of  :description
  validates_presence_of  :user
end